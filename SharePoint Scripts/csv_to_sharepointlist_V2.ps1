#Connect
Connect-PnPOnline –Url YOUR_SHAREPOINT_WEBSITE –UseWebLogin


#Remove ALL Items from the named list
$items = Get-PnPListItem -List "LIST_AS_STRING" -PageSize 1000
foreach($item in $items) {
    try {
        Remove-PnPListItem -List "LIST_AS_STRING" -Identity $item.Id -Force
    } catch {
        Write-Host "error"
    }
}


#Set path to the CSV
$path = "PATH_TO_CSV\file.csv"
$csvs = Import-Csv $path -Delimiter (";")

#Import CSV Items
foreach($values in $csvs ) {
    try {
        Add-PnPListItem -List "vertretung" -ContentType $values.ContentType -Values @{
            "SHAREPOINTLISTITEM_AS_STRING" = $values.CSVITEM [;
            "SHAREPOINTLISTITEM_AS_STRING" = $values.CSVITEM ]
        }
    } catch {
        Write-Host "error"
    }
    $values
}

#Disconnect
Disconnect-PnPOnline